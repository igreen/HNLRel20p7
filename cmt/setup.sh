# echo "setup HNL HNL-00-00-00 in /afs/cern.ch/work/j/jbiswal/NewCode_HNL"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc49-opt/2.4.33/CMT/v1r25p20160527; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtHNLtempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtHNLtempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=HNL -version=HNL-00-00-00 -path=/afs/cern.ch/work/j/jbiswal/NewCode_HNL  -no_cleanup $* >${cmtHNLtempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=HNL -version=HNL-00-00-00 -path=/afs/cern.ch/work/j/jbiswal/NewCode_HNL  -no_cleanup $* >${cmtHNLtempfile}"
  cmtsetupstatus=2
  /bin/rm -f ${cmtHNLtempfile}
  unset cmtHNLtempfile
  return $cmtsetupstatus
fi
cmtsetupstatus=0
. ${cmtHNLtempfile}
if test $? != 0 ; then
  cmtsetupstatus=2
fi
/bin/rm -f ${cmtHNLtempfile}
unset cmtHNLtempfile
return $cmtsetupstatus

