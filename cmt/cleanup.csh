# echo "cleanup HNL HNL-00-00-00 in /afs/cern.ch/work/j/jbiswal/NewCode_HNL"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc49-opt/2.4.33/CMT/v1r25p20160527
endif
source ${CMTROOT}/mgr/setup.csh
set cmtHNLtempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtHNLtempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=HNL -version=HNL-00-00-00 -path=/afs/cern.ch/work/j/jbiswal/NewCode_HNL  $* >${cmtHNLtempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=HNL -version=HNL-00-00-00 -path=/afs/cern.ch/work/j/jbiswal/NewCode_HNL  $* >${cmtHNLtempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmtHNLtempfile}
  unset cmtHNLtempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmtHNLtempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmtHNLtempfile}
unset cmtHNLtempfile
exit $cmtcleanupstatus

