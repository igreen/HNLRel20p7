/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef HNL_HNL_H
#define HNL_HNL_H 1

#include <xAODTruth/TruthParticle.h>
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/ElectronContainer.h"
#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/Vertex.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTruth/TruthParticle.h"
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ITHistSvc.h"
#include "StoreGate/StoreGateSvc.h"
#include "xAODCore/ShallowCopy.h"

#include "TrigDecisionTool/TrigDecisionTool.h"
#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h"

#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"


#include "TTree.h"

class TH1I;

class TH1F;

class TEfficiency;

using namespace std;

class HNL : public ::AthAnalysisAlgorithm {
public:
		HNL(const std::string &name, ISvcLocator *pSvcLocator);

		virtual ~HNL();

		virtual StatusCode initialize();     //once, before any input is loaded
		virtual StatusCode beginInputFile(); //start of each input file, only metadata loaded
//    virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
		virtual StatusCode execute(); //per event
		//virtual StatusCode  endInputFile();   //end of each input file
		//virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
		virtual StatusCode finalize(); //once, after all events processed

private:
		StoreGateSvc *m_storeGate;
		ToolHandle<Trig::TrigDecisionTool> m_trigDec;

//    // MuonCalibrationAndSmearing
		CP::MuonCalibrationAndSmearingTool *m_muonCalibrationAndSmearingTool; //!
		CP::MuonSelectionTool *m_muonLooseSelection; //!
		CP::MuonSelectionTool *m_muonMediumSelection; //!
		CP::MuonSelectionTool *m_muonTightSelection; //
		AsgElectronLikelihoodTool *m_TightLH; //!


		bool isLeadingMu(xAOD::Muon *leadingMuon);

		bool isSubleadingMu(xAOD::Muon *subLeadingMuon);

		bool testDV(const xAOD::Vertex *displacedVertex);

		void resetParams();

		StatusCode findSignature();

		xAOD::Muon *matchTrackToMuon(const xAOD::TrackParticle *track);

		xAOD::Electron *matchTrackToElectron(const xAOD::TrackParticle *track);

		ITHistSvc *m_thistSvc;
		TH1F *cutFlow;
		TTree *hnlTree = nullptr;

		// Prompt leading muon (denoted as 'Mu')
		Double_t m_MuPt = -999; // Transverse momentum of the prompt leading muon
		Double_t m_MuEta = -999; // Eta of the prompt leading muon
		Double_t m_MuPhi = -999; // Phi of the prompt leading muon
		Double_t m_MuPx = -999; // Px of the prompt leading muon
		Double_t m_MuPy = -999; // Py of the prompt leading muon
		Double_t m_MuPz = -999; // Pz of the prompt leading muon
		Double_t m_MuPtC = -999; // PtCone30 of the prompt leading muon
		Double_t m_Mud0 = -999; // d0 of the prompt leading muon
		Double_t m_MuMass = -999;


		// The first sub-leading lepton (denoted as 'Lep1')
		Double_t m_Lep1Pt = -999; // Transverse momentum of the first sub-leading lepton
		Double_t m_Lep1Eta = -999; // Eta of the first sub-leading lepton
		Double_t m_Lep1Phi = -999; // Phi of the first sub-leading lepton
		Double_t m_Lep1Px = -999; // Px of the first sub-leading lepton
		Double_t m_Lep1Py = -999; // Py of the first sub-leading lepton
		Double_t m_Lep1Pz = -999; // Pz of the first sub-leading lepton
		Double_t m_Lep1PtC = -999; // PtCone30 of the first sub-leading lepton
		Double_t m_Lep1d0 = -999; // d0 of the first sub-leading lepton
		Double_t m_Lep1Mass = -999;

		// The second sub-leading lepton (denoted as 'Lep2')
		Double_t m_Lep2Pt = -999; // Transverse momentum of the second sub-leading lepton
		Double_t m_Lep2Eta = -999; // Eta of the second sub-leading lepton
		Double_t m_Lep2Phi = -999; // Phi of the second sub-leading lepton
		Double_t m_Lep2Px = -999; // Px of the second sub-leading lepton
		Double_t m_Lep2Py = -999; // Py of the second sub-leading lepton
		Double_t m_Lep2Pz = -999; // Pz of the second sub-leading lepton
		Double_t m_Lep2PtC = -999; // PtCone30 of the second sub-leading lepton
		Double_t m_Lep2d0 = -999; // d0 of the second sub-leading lepton
		Double_t m_Lep2Mass = -999;
		// The sub-leading electron (denoted as 'El')
		Double_t m_ElePt = -999; // Transverse momentum of the second sub-leading electron
		Double_t m_EleEta = -999; // Eta of the second sub-leading electron
		Double_t m_ElePhi = -999; // Phi of the second sub-leading electron
		Double_t m_ElePx = -999; // Px of the second sub-leading electron
		Double_t m_ElePy = -999; // Py of the second sub-leading electron
		Double_t m_ElePz = -999; // Pz of the second sub-leading electron
		Double_t m_Eled0 = -999; // d0 of the second sub-leading electron

		// HNL
		Double_t m_HNLrDV = -999; // rDV of the heavy neutral lepton (HNL)

		// DV
		Double_t m_DVMass = -999; // Mass of the displaced vertex (DV)

		//Truth variables
		Int_t m_WChargeTruth = -999;
		Int_t m_WPdgIDTruth = -999; // PDG ID of the prompt leading muon
		Double_t m_WMassTruth = -999;
		Double_t m_WDecayVtx = -999;

		// Prompt leading muon (denoted as 'Mu')
		Double_t m_MuPtTruth = -999; // Transverse momentum of the prompt leading muon
		Double_t m_MuEtaTruth = -999; // Eta of the prompt leading muon
		Double_t m_MuPhiTruth = -999; // Phi of the prompt leading muon
		Int_t m_MuChargeTruth = -999; // Charge of the prompt leading muon
		Int_t m_MuPdgIDTruth = -999; // PDG ID of the prompt leading muon
		Double_t m_MuPxTruth = -999; // Transverse momentum of the prompt leading muon
		Double_t m_MuPyTruth = -999; // Eta of the prompt leading muon
		Double_t m_MuPzTruth = -999; // Phi of the prompt leading muon
		Double_t m_MuMassTruth = -999;

		// The first sub-leading muon (denoted as 'Mu1')
		Double_t m_Lep1PtTruth = -999; // Transverse momentum of the first sub-leading muon
		Double_t m_Lep1EtaTruth = -999; // Eta of the first sub-leading muon
		Double_t m_Lep1PhiTruth = -999; // Phi of the first sub-leading muon
		Double_t m_Lep1d0Truth = -999; // d0 of the first sub-leading muon
		Int_t m_Lep1ChargeTruth = -999; // Charge of the prompt leading muon
		Int_t m_Lep1PdgIDTruth = -999; // PDG ID of the prompt leading muon
		Double_t m_Lep1PxTruth = -999; // Transverse momentum of the prompt leading muon
		Double_t m_Lep1PyTruth = -999; // Eta of the prompt leading muon
		Double_t m_Lep1PzTruth = -999; // Phi of the prompt leading muon
		Double_t m_Lep1MassTruth = -999;

		// The second sub-leading muon (denoted as 'Lep2')
		Double_t m_Lep2PtTruth = -999; // Transverse momentum of the second sub-leading muon
		Double_t m_Lep2EtaTruth = -999; // Eta of the second sub-leading muon
		Double_t m_Lep2PhiTruth = -999; // Phi of the second sub-leading muon
		Double_t m_Lep2d0Truth = -999; // d0 of the second sub-leading muon
		Int_t m_Lep2ChargeTruth = -999; // Charge of the prompt leading muon
		Int_t m_Lep2PdgIDTruth = -999; // PDG ID of the prompt leading muon
		Double_t m_Lep2PxTruth = -999; // Transverse momentum of the prompt leading muon
		Double_t m_Lep2PyTruth = -999; // Eta of the prompt leading muon
		Double_t m_Lep2PzTruth = -999; // Phi of the prompt leading muon
		Double_t m_Lep2MassTruth = -999;

		// HNL
		Double_t m_HNLrDVTruth = -999; // rDV of the heavy neutral lepton (HNL)
		Int_t m_HNLPdgIDTruth = -999; // PDG ID of the prompt leading muon
		Double_t m_HNLMassTruth = -999;

		//DVmass
		Double_t m_DVMassTruth = -999;


		//cuts values in truth events
		Double_t m_CosmicVeto = -999;

		Int_t m_cosmicPassed = -999;

		int m_eventNum = 0;

		// Monte Carlo sample check
		bool isMC = false;

		//Event Selection
		//DRAW Filter
		bool PassTrigger = false; //Done
		bool PassPromtMu = false; //Done
		bool PassDispMu = false; //Done

		bool PassDRAW = false; //Done

		//Signal (A)
		bool PassTightPromptMuon = false;
		bool PassMaterialVeto = false;
		bool PassFiducialCut = false;
		bool Pass2TrackDV = false;
		bool Pass2TrackOSDV = false;
		bool Pass1MuonDV = false;
		bool Pass1TightMuonDV = false;
		bool PassTightElectron = false;
		bool Pass2LeptonDV = false;
		bool Pass2TightLeptonDV = false;
		bool PassCosmicVeto = false;
		bool PassDVMassCut = false;

		bool PassDVPresence = false;


		bool variousTests = false;


};

#endif //> !HNL_HNLALG_H
