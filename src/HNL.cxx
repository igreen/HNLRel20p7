/*
 * Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
 *
 * @author Itamar Green <itamar.benjamin.green@cern.ch>
 */


// HNL includes
#include "HNL.h"

#include "xAODEventInfo/EventInfo.h"

#include "xAODEgamma/Egamma.h"
#include "xAODEgamma/Electron.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronxAODHelpers.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthVertexContainer.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

#include <cmath>
#include <GaudiKernel/IPartPropSvc.h>
#include <xAODCore/ShallowAuxContainer.h>
#include <xAODCore/ShallowCopy.h>
#include <MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h>
#include <DataModel/DataVector.h>
#include <AthContainers/DataVector.h>


const double PI = 3.141592653589793116;
const double TWOPI = 2.0 * PI;

inline double delta_phi(double phi1, double phi2) {
	double PHI = phi1 - phi2;
	int isign = (PHI < 0.0) ? -1 : 1;
	double absphi = (fabs(PHI) <= PI) ? fabs(PHI) : TWOPI - fabs(PHI);
	double newphi = (isign < 0) ? -absphi : absphi;
	return newphi;
}

double delta_R(double eta1, double phi1, double eta2, double phi2) {
	double deltaR = sqrt((eta1 - eta2) * (eta1 - eta2) + delta_phi(phi1, phi2) * delta_phi(phi1, phi2));
	return deltaR;
}


HNL::HNL(const std::string &name, ISvcLocator *pSvcLocator) : AthAnalysisAlgorithm(name, pSvcLocator),
                                                              m_trigDec("Trig::TrigDecisionTool/TrigDecisionTool") {}

HNL::~HNL() {}

StatusCode HNL::initialize() {
	ATH_MSG_INFO("Initializing " << name() << "...");
	using namespace std;

	StatusCode sc = service("StoreGateSvc", m_storeGate);
	if (sc.isFailure()) {
		ATH_MSG_ERROR("Unable to retrieve pointer to StoreGateSvc");
		return sc;
	}
					ATH_CHECK(m_trigDec.retrieve());
	sc = service("THistSvc", m_thistSvc);
	if (sc.isFailure()) {
		ATH_MSG_ERROR("Unable to retrieve pointer to THistSvc");
		return sc;
	}
	// Histograming...
	ServiceHandle<ITHistSvc> histSvc("THistSvc", name());
					ATH_CHECK(histSvc.retrieve());

	if (sc.isFailure()) {
		ATH_MSG_ERROR("Unable to register histogramming service");
		return sc;
	}

	m_muonCalibrationAndSmearingTool = new CP::MuonCalibrationAndSmearingTool("MuonCorrectionTool");
					ATH_CHECK(m_muonCalibrationAndSmearingTool->initialize());

	m_muonLooseSelection = new CP::MuonSelectionTool("MuonSelectionL");
					ATH_CHECK(m_muonLooseSelection->setProperty("MaxEta", 2.5));
					ATH_CHECK(m_muonLooseSelection->setProperty("MuQuality", 2));
					ATH_CHECK(m_muonLooseSelection->initialize());

	m_muonMediumSelection = new CP::MuonSelectionTool("MuonSelectionM");
					ATH_CHECK(m_muonMediumSelection->setProperty("MaxEta", 2.5));
					ATH_CHECK(m_muonMediumSelection->setProperty("MuQuality", 1));
					ATH_CHECK(m_muonMediumSelection->initialize());

	m_muonTightSelection = new CP::MuonSelectionTool("MuonSelectionT");
					ATH_CHECK(m_muonTightSelection->setProperty("MaxEta", 2.5));
					ATH_CHECK(m_muonTightSelection->setProperty("MuQuality", 0));
					ATH_CHECK(m_muonTightSelection->initialize());

	m_TightLH = new AsgElectronLikelihoodTool("TightLH");
					ATH_CHECK(m_TightLH->setProperty("WorkingPoint", "TightLHNod0Electron"));
					ATH_CHECK(m_TightLH->initialize());
					ATH_CHECK(m_TightLH->setProperty("CutBL", std::vector<int>()));
					ATH_CHECK(m_TightLH->setProperty("CutPi", std::vector<int>()));


	hnlTree = new TTree("HNLTree", "Hnl tree");
	cutFlow = new TH1F("CutFlow", "cut flow tree", 20, 0, 20);

	CHECK(histSvc->regTree("/HNL/m_hnlTree", hnlTree));
	CHECK(histSvc->regHist("/HNL/m_cutFlow", cutFlow));

	// Branches for the prompt leading muon
	hnlTree->Branch("MuPt", &m_MuPt, "MuPt/D");
	hnlTree->Branch("MuEta", &m_MuEta, "MuEta/D");
	hnlTree->Branch("MuPhi", &m_MuPhi, "MuPhi/D");
	hnlTree->Branch("MuPx", &m_MuPx, "MuPx/D");
	hnlTree->Branch("MuPy", &m_MuPy, "MuPy/D");
	hnlTree->Branch("MuPz", &m_MuPz, "MuPz/D");
	hnlTree->Branch("MuPtC", &m_MuPtC, "MuPtC/D");
	hnlTree->Branch("Mud0", &m_Mud0, "Mud0/D");
	hnlTree->Branch("MuMass", &m_MuMass, "MuMass/D");

	// Branches for the first sub-leading lepton
	hnlTree->Branch("Lep1Pt", &m_Lep1Pt, "Lep1Pt/D");
	hnlTree->Branch("Lep1Eta", &m_Lep1Eta, "Lep1Eta/D");
	hnlTree->Branch("Lep1Phi", &m_Lep1Phi, "Lep1Phi/D");
	hnlTree->Branch("Lep1Px", &m_Lep1Px, "Lep1Px/D");
	hnlTree->Branch("Lep1Py", &m_Lep1Py, "Lep1Py/D");
	hnlTree->Branch("Lep1Pz", &m_Lep1Pz, "Lep1Pz/D");
	hnlTree->Branch("Lep1PtC", &m_Lep1PtC, "Lep1PtC/D");
	hnlTree->Branch("Lep1d0", &m_Lep1d0, "Lep1d0/D");
	hnlTree->Branch("Lep1Mass", &m_Lep1Mass, "Lep1Mass/D");

	// Branches for the second sub-leading lepton
	hnlTree->Branch("Lep2Pt", &m_Lep2Pt, "Lep2Pt/D");
	hnlTree->Branch("Lep2Eta", &m_Lep2Eta, "Lep2Eta/D");
	hnlTree->Branch("Lep2Phi", &m_Lep2Phi, "Lep2Phi/D");
	hnlTree->Branch("Lep2Px", &m_Lep2Px, "Lep2Px/D");
	hnlTree->Branch("Lep2Py", &m_Lep2Py, "Lep2Py/D");
	hnlTree->Branch("Lep2Pz", &m_Lep2Pz, "Lep2Pz/D");
	hnlTree->Branch("Lep2PtC", &m_Lep2PtC, "Lep2PtC/D");
	hnlTree->Branch("Lep2d0", &m_Lep2d0, "Lep2d0/D");
	hnlTree->Branch("Lep2Mass", &m_Lep2Mass, "Lep2Mass/D");

	// Branches for the HNL
	hnlTree->Branch("HNLrDV", &m_HNLrDV, "HNLrDV/D");

	// Branches for the DV
	hnlTree->Branch("DVMass", &m_DVMass, "DVMass/D");

	//Truth Branches


	hnlTree->Branch("WChargeTruth", &m_WChargeTruth, "WChargeTruth/I");
	hnlTree->Branch("WPdgIDTruth", &m_WPdgIDTruth, "WPdgIDTruth/I");
	hnlTree->Branch("WMassTruth", &m_WMassTruth, "WMassTruth/D");

	// Branches for the prompt leading muon
	hnlTree->Branch("MuPtTruth", &m_MuPtTruth, "MuPtTruth/D");
	hnlTree->Branch("MuEtaTruth", &m_MuEtaTruth, "MuEtaTruth/D");
	hnlTree->Branch("MuPhiTruth", &m_MuPhiTruth, "MuPhiTruth/D");
	hnlTree->Branch("MuChargeTruth", &m_MuChargeTruth, "MuChargeTruth/I");
	hnlTree->Branch("MuPdgIDTruth", &m_MuPdgIDTruth, "MuPdgIDTruth/I");
	hnlTree->Branch("MuPxTruth", &m_MuPxTruth, "MuPxTruth/D");
	hnlTree->Branch("MuPyTruth", &m_MuPyTruth, "MuPyTruth/D");
	hnlTree->Branch("MuPzTruth", &m_MuPzTruth, "MuPzTruth/D");
	hnlTree->Branch("MuMassTruth", &m_MuMassTruth, "MuMassTruth/D");

	// Branches for the first sub-leading muon
	hnlTree->Branch("Lep1PtTruth", &m_Lep1PtTruth, "Lep1PtTruth/D");
	hnlTree->Branch("Lep1EtaTruth", &m_Lep1EtaTruth, "Lep1EtaTruth/D");
	hnlTree->Branch("Lep1PhiTruth", &m_Lep1PhiTruth, "Lep1PhiTruth/D");
	hnlTree->Branch("Lep1d0Truth", &m_Lep1d0Truth, "Lep1d0Truth/D");
	hnlTree->Branch("Lep1ChargeTruth", &m_Lep1ChargeTruth, "Lep1ChargeTruth/I");
	hnlTree->Branch("Lep1PdgIDTruth", &m_Lep1PdgIDTruth, "Lep1PdgIDTruth/I");
	hnlTree->Branch("Lep1PxTruth", &m_Lep1PxTruth, "Lep1PxTruth/D");
	hnlTree->Branch("Lep1PyTruth", &m_Lep1PyTruth, "Lep1PyTruth/D");
	hnlTree->Branch("Lep1PzTruth", &m_Lep1PzTruth, "Lep1PzTruth/D");
	hnlTree->Branch("Lep1MassTruth", &m_Lep1MassTruth, "Lep1MassTruth/D");

	// Branches for the second sub-leading muon
	hnlTree->Branch("Lep2PtTruth", &m_Lep2PtTruth, "Lep2PtTruth/D");
	hnlTree->Branch("Lep2EtaTruth", &m_Lep2EtaTruth, "Lep2EtaTruth/D");
	hnlTree->Branch("Lep2PhiTruth", &m_Lep2PhiTruth, "Lep2PhiTruth/D");
	hnlTree->Branch("Lep2d0Truth", &m_Lep2d0Truth, "Lep2d0Truth/D");
	hnlTree->Branch("Lep2ChargeTruth", &m_Lep2ChargeTruth, "Lep2ChargeTruth/I");
	hnlTree->Branch("Lep2PdgIDTruth", &m_Lep2PdgIDTruth, "Lep2PdgIDTruth/I");
	hnlTree->Branch("Lep2PxTruth", &m_Lep2PxTruth, "Lep2PxTruth/D");
	hnlTree->Branch("Lep2PyTruth", &m_Lep2PyTruth, "Lep2PyTruth/D");
	hnlTree->Branch("Lep2PzTruth", &m_Lep2PzTruth, "Lep2PzTruth/D");
	hnlTree->Branch("Lep2MassTruth", &m_Lep2MassTruth, "Lep2MassTruth/D");

	// Branches for the HNL
	hnlTree->Branch("HNLrDVTruth", &m_HNLrDVTruth, "HNLrDVTruth/D");
	hnlTree->Branch("HNLPdgIDTruth", &m_HNLPdgIDTruth, "HNLPdgIDTruth/I");
	hnlTree->Branch("HNLMassTruth", &m_HNLMassTruth, "HNLMassTruth/D");

	// Branches for the DV
	hnlTree->Branch("DVMassTruth", &m_DVMassTruth, "DVMassTruth/D");

	hnlTree->Branch("CosmicVetoTruth", &m_CosmicVeto, "CosmicVetoTruth/D");
	hnlTree->Branch("CosmicPass", &m_cosmicPassed, "cosmicCutPass/I");

	return StatusCode::SUCCESS;
}


StatusCode HNL::beginInputFile() {

	return StatusCode::SUCCESS;
}

StatusCode HNL::finalize() {
	ATH_MSG_INFO("Finalizing " << name() << "...");

	return StatusCode::SUCCESS;
}


StatusCode HNL::execute() {
	StatusCode statusCode = StatusCode::SUCCESS;
	ATH_MSG_DEBUG("Executing " << name() << "...");
	setFilterPassed(false); //optional: start with algorithm not passed
	resetParams();
	const xAOD::EventInfo *eventInfo = 0;
	CHECK(evtStore()->retrieve(eventInfo));

	m_eventNum++;


	isMC = true;
	if (!eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)) {
		isMC = false;
	}
	if (m_trigDec->isPassed("HLT_mu26_ivarmedium")) {
		PassTrigger = true;
	}
	if (PassTrigger) {
		statusCode = findSignature();
	}

	cutFlow->Fill(0);
	if (PassTrigger) {
		cutFlow->Fill(1);
		if (PassDRAW) {
			cutFlow->Fill(2);
			if (PassTightPromptMuon) {
				cutFlow->Fill(3);
				if (PassDVPresence) {
					cutFlow->Fill(4);
					if (PassFiducialCut) {
						cutFlow->Fill(5);
						if (Pass2TrackDV) {
							cutFlow->Fill(6);
							if (Pass2TrackOSDV) {
								cutFlow->Fill(7);
								if (Pass1MuonDV) {
									cutFlow->Fill(8);
								}
							}
						}
					}
				}
			}
		}
	}


	setFilterPassed(true);


	return statusCode;
}


StatusCode HNL::findSignature() {
	StatusCode resultsSC = StatusCode::SUCCESS;
	const xAOD::MuonContainer *muons = 0;
	resultsSC = m_storeGate->retrieve(muons, "Muons");
	const xAOD::VertexContainer *secVerts = 0;
	resultsSC = m_storeGate->retrieve(secVerts, "VrtSecInclusive_SecondaryVertices");

	const xAOD::TruthParticleContainer *truthParticles = 0;
	resultsSC = m_storeGate->retrieve(truthParticles, "TruthParticles");

	std::vector<int> prompMu_barcode;
	std::vector<int> dispLep1_barcode;
	std::vector<int> dispLep2_barcode;
	std::vector<int> dispLep_barcode;
	std::vector<int> dispEl_barcode;
	std::vector<int> dispElTrk_barcode;
	std::vector<int> dispVtx_barcode;

	//for MC events:
	if (isMC) {
		const xAOD::TrackParticleContainer *recoInDetTracks = 0;
		m_storeGate->retrieve(recoInDetTracks, "InDetTrackParticles");
		for (xAOD::TrackParticleContainer::const_iterator recoIDTrk_itr = recoInDetTracks->begin(); recoIDTrk_itr != recoInDetTracks->end(); recoIDTrk_itr++) {
			const xAOD::TruthParticle *Trk_truth = xAOD::TruthHelpers::getTruthParticle(**recoIDTrk_itr);
			int Trk_barcode = 0;
			if (Trk_truth) {
				Trk_barcode = Trk_truth->barcode();
			}

		}


		const xAOD::TruthVertex *WDecayVtx = 0;
		const xAOD::TruthVertex *displacedVtx = 0;
		const xAOD::TruthParticle *HNLtruth = 0;
		TLorentzVector displacedLepton1(0, 0, 0, 0);
		TLorentzVector displacedLepton2(0, 0, 0, 0);
		TLorentzVector hnlP4(0, 0, 0, 0);
		bool hasPromptMu = false;
		bool diLeptonDecay = false;
		for (xAOD::TruthParticleContainer::const_iterator truth_itr = truthParticles->begin(); truth_itr != truthParticles->end(); truth_itr++) {
			const xAOD::TruthParticle *truthParticle = *truth_itr;
			if (truthParticle->absPdgId() == 24) {

				//this is a W
				if (truthParticle->hasDecayVtx()) {
					//we store its decay vertex
					WDecayVtx = truthParticle->decayVtx();
					m_WChargeTruth = static_cast<Int_t>(truthParticle->charge());
					m_WPdgIDTruth = truthParticle->pdgId();
					m_WMassTruth = truthParticle->m();
					if (WDecayVtx->nOutgoingParticles() == 2) {
						//this W decays into 2 particles. We'll check if they have pdgID 50 and 13 (HNL and muon, respectively)
						for (size_t childItr = 0; childItr < WDecayVtx->nOutgoingParticles(); childItr++) {
							const xAOD::TruthParticle *childParticle = WDecayVtx->outgoingParticle(childItr);
							if (!childParticle) { continue; }
							if (childParticle->absPdgId() == 13 && childParticle->charge() == truthParticle->charge()) {
								//child is muon.
								hasPromptMu = true;
								m_MuPtTruth = childParticle->pt() / 1000;
								m_MuEtaTruth = childParticle->eta();
								m_MuPhiTruth = childParticle->phi();
								m_MuChargeTruth = static_cast<Int_t>(childParticle->charge());
								m_MuPxTruth = childParticle->px() / 1000;
								m_MuPyTruth = childParticle->py() / 1000;
								m_MuPzTruth = childParticle->pz() / 1000;
								m_MuPdgIDTruth = childParticle->pdgId();
								m_MuMassTruth = childParticle->m() / 1000;
								prompMu_barcode.push_back(childParticle->barcode());
							} else {
//								ATH_MSG_INFO("Other daughter PDG ID: " << childParticle->pdgId() << " in event " << m_eventNum);
								if (childParticle->pdgId() == 50) {
									HNLtruth = childParticle;
									hnlP4 = HNLtruth->p4();
									if (HNLtruth->hasDecayVtx()) {
										//we store its decay vertex
										displacedVtx = HNLtruth->decayVtx();
										dispVtx_barcode.push_back(displacedVtx->barcode());
										double WHNLxDiff = 0;
										double WHNLyDiff = 0;
										WHNLxDiff = (WDecayVtx->x()) - displacedVtx->x();
										WHNLyDiff = (WDecayVtx->y()) - displacedVtx->y();


										m_HNLrDVTruth = sqrt((displacedVtx->x()) * (displacedVtx->x()) + (displacedVtx->y()) * (displacedVtx->y()));
										m_HNLMassTruth = HNLtruth->m();
										m_HNLPdgIDTruth = HNLtruth->pdgId();

										if (displacedVtx->nOutgoingParticles() == 3) {
											const xAOD::TruthParticle *chargedLep1 = 0;
											const xAOD::TruthParticle *chargedLep2 = 0;
											const xAOD::TruthParticle *neutrino = 0;


											for (size_t childItrHNL = 0; childItrHNL < displacedVtx->nOutgoingParticles(); childItrHNL++) {
												const xAOD::TruthParticle *hnlChildParticle = displacedVtx->outgoingParticle(childItr);
												if (!hnlChildParticle) { continue; }
												if (hnlChildParticle->isLepton()) {
													dispLep_barcode.push_back(hnlChildParticle->barcode());
												}
												if (hnlChildParticle->pdgId() == 11 || hnlChildParticle->pdgId() == -11) {
													dispEl_barcode.push_back(hnlChildParticle->barcode());
												}
												if (hnlChildParticle->isNeutrino()) {
													neutrino = hnlChildParticle;
												} else {
													if (chargedLep1 != 0) {
														chargedLep2 = hnlChildParticle;
													} else {
														chargedLep1 = hnlChildParticle;
													}
												}
											}
											m_Lep1ChargeTruth = static_cast<Int_t>(chargedLep1->charge());
											m_Lep2ChargeTruth = static_cast<Int_t>(chargedLep2->charge());
											dispLep1_barcode.push_back(chargedLep1->barcode());
											dispLep2_barcode.push_back(chargedLep2->barcode());



//											&& m_Lep2ChargeTruth == -1) || (m_Lep1ChargeTruth == -1 && m_Lep2ChargeTruth == 1)
//											if (chargedLep1->charge() == -chargedLep2->charge()) {

											TLorentzVector dispLepP4 = chargedLep1->p4();
											double lep1Px = dispLepP4.Px();
											double lep1Py = dispLepP4.Py();
											double lep1Pz = dispLepP4.Pz();
											double d0 = (WHNLyDiff - (lep1Py / lep1Px) * WHNLxDiff) /
											            sqrt(1.0f + (lep1Py / lep1Px) * (lep1Py / lep1Px));

											displacedLepton1 = dispLepP4;
											m_Lep1PtTruth = chargedLep1->pt() / 1000;
											m_Lep1EtaTruth = chargedLep1->eta();
											m_Lep1PhiTruth = chargedLep1->phi();
											m_Lep1d0Truth = d0;
											m_Lep1PxTruth = lep1Px / 1000;
											m_Lep1PyTruth = lep1Py / 1000;
											m_Lep1PzTruth = lep1Pz / 1000;
											m_Lep1PdgIDTruth = chargedLep1->pdgId();
											m_Lep1MassTruth = chargedLep1->m() / 1000;

											TLorentzVector dispLep2P4 = chargedLep2->p4();
											double lep2Px = dispLep2P4.Px();
											double lep2Py = dispLep2P4.Py();
											double lep2Pz = dispLep2P4.Pz();
											double lep2d0 = (WHNLyDiff - (lep2Py / lep2Px) * WHNLxDiff) /
											                sqrt(1.0f + (lep2Py / lep2Px) * (lep2Py / lep2Px));

											diLeptonDecay = true;
											displacedLepton2 = dispLep2P4;
											m_Lep2PtTruth = chargedLep2->pt() / 1000;
											m_Lep2EtaTruth = chargedLep2->eta();
											m_Lep2PhiTruth = chargedLep2->phi();
											m_Lep2d0Truth = lep2d0;
											m_Lep2PxTruth = lep2Px / 1000;
											m_Lep2PyTruth = lep2Py / 1000;
											m_Lep2PzTruth = lep2Pz / 1000;
											m_Lep2PdgIDTruth = chargedLep2->pdgId();
											m_Lep2MassTruth = chargedLep2->m() / 1000;
//											}

										}
									}//end of HNL decay vertex block
								}//end of "If HNL"
							}
						}//end of W daughters loop
					}//end of 2-daughter W decay
				}// end W decay vertex block
			}//end of "is W"

		}//End of truth particle loop
		if (diLeptonDecay) {
			double etaSum = displacedLepton1.Eta() + displacedLepton2.Eta();
			double deltaPhi = displacedLepton1.DeltaPhi(displacedLepton2);
			double cosmic = sqrt((etaSum * etaSum) - (M_PI - deltaPhi) * (M_PI - deltaPhi));
			m_CosmicVeto = cosmic;
			if (cosmic > 0.04) {
				m_cosmicPassed++;
			}
		}
	}//End of Truth Section

	const xAOD::Vertex *pvx = 0;
	const xAOD::VertexContainer *recoPVertices = 0;
	m_storeGate->retrieve(recoPVertices, "PrimaryVertices");

	//get primary vertex
	for (xAOD::VertexContainer::const_iterator pdv_itr = recoPVertices->begin();
	     pdv_itr != recoPVertices->end(); pdv_itr++) {
		if ((*pdv_itr)->vertexType() == xAOD::VxType::PriVtx) {
			pvx = *pdv_itr;
			//      PVX_exist = true;
		}
	}
	std::pair<xAOD::MuonContainer *, xAOD::ShallowAuxContainer *> muons_shallowCopy = xAOD::shallowCopyContainer(*muons);


	xAOD::MuonContainer::iterator lead_itr = (muons_shallowCopy.first)->begin();
	xAOD::MuonContainer::iterator disp_mu = (muons_shallowCopy.first)->begin();

	for (; lead_itr != (muons_shallowCopy.first)->end(); lead_itr++) {
		xAOD::Muon *mu = *lead_itr;
		if (!isLeadingMu(mu)) {
			continue;
		}
		PassPromtMu = true;

		if (!PassDispMu) {
			for (; disp_mu != (muons_shallowCopy.first)->end(); disp_mu++) {
				//skip the leading muon
				if (disp_mu == lead_itr)continue;
				xAOD::Muon *dispMu = *disp_mu;
				if (isSubleadingMu(dispMu)) {
					PassDispMu = true;
					break;
				}
			}
		}

		//We can only get to this line if the event passes the trigger, so we just check if there exist a prompt muon and a displaced muon who pass the cuts.
		PassDRAW = (PassPromtMu && PassDispMu);

		if (m_muonCalibrationAndSmearingTool->applyCorrection(**lead_itr) == CP::CorrectionCode::Error) {
			Error("execute()", "MuonCalibrationAndSmearingTool returns Error CorrectionCode");
		}
		if (m_muonTightSelection->accept(**lead_itr)) {
			const xAOD::TrackParticle *trk = (*lead_itr)->primaryTrackParticle();
			if (pvx != nullptr) {
				double d0_sig = fabs(trk->d0()) / sqrt(trk->definingParametersCovMatrix()(0, 0));
				double delta_z0 = fabs(trk->z0() + trk->vz() - pvx->z());
				if (fabs(d0_sig) < 3 && delta_z0 * sin(trk->theta()) < 0.5) {
					PassTightPromptMuon = true;
				}
			}
		}
		if (!PassDVPresence) {
			if (secVerts->size() > 0) {
				PassDVPresence = true;
			}
		}

		for (xAOD::VertexContainer::const_iterator vtx_itr = secVerts->begin();
		     vtx_itr != secVerts->end(); ++vtx_itr) {

			bool has1Mu = false;
			bool has2Mu = false;
			bool has2Ele = false;
			bool hasEleMu = false;
			const xAOD::Vertex *vertex = *vtx_itr;
			testDV(vertex);
			if (vertex->nTrackParticles() < 2) { continue; }
			const xAOD::Muon *subMuOpt1 = 0;
			const xAOD::Muon *subMuOpt2 = 0;
			const xAOD::Electron *subEleOpt1 = 0;
			const xAOD::Electron *subEleOpt2 = 0;
			for (size_t tk = 0; tk < (*vtx_itr)->nTrackParticles(); ++tk) {
				const xAOD::TrackParticle *dvTrk = (*vtx_itr)->trackParticle(tk);
				xAOD::Muon *matchedMuon = matchTrackToMuon(dvTrk);
				xAOD::Electron *matchedEle = matchTrackToElectron(dvTrk);
				if (matchedMuon != nullptr) {
					if (isSubleadingMu(matchedMuon)) {
						if (subMuOpt1 == 0) {
							subMuOpt1 = matchedMuon;
						} else {
							subMuOpt2 = matchedMuon;
						}
//						if (!has1Mu) {
//							has1Mu = true;
//							subMuOpt1 = matchedMuon;
//						} else {
//							has2Mu = true;
//							subMuOpt2 = matchedMuon;
//						}
					}
				}
				if (matchedEle != nullptr) {
//					hasEle = true;
//					subEleOpt = matchedEle;
					if (subEleOpt1 == 0) {
						subEleOpt1 = matchedEle;
					} else {
						subEleOpt2 = matchedEle;
					}
				}
			}
			if (subMuOpt1 != 0 && subMuOpt2 != 0) { has2Mu = true; }
			if (subMuOpt1 != 0 && subEleOpt1 != 0) { hasEleMu = true; }
			if (subEleOpt1 != 0 && subEleOpt2 != 0) { has2Ele = true; }

			if (has2Mu || has2Ele || hasEleMu) {
				//now we need to check if this is an MC event. If so, we should do something with truth information

				float displacedVtxX = vertex->x();
				float displacedVtxY = vertex->y();

				m_DVMass = vertex->auxdataConst<float>("vtx_mass");

				m_HNLrDV = sqrt(displacedVtxX * displacedVtxX + displacedVtxY * displacedVtxY);

				m_MuPt = mu->pt() / 1000;
				m_MuEta = mu->eta();
				m_MuPhi = mu->phi();
				m_MuPtC = mu->isolation(xAOD::Iso::ptcone30) / 1000;
				m_MuPx = mu->p4().Px();
				m_MuPy = mu->p4().Py();
				m_MuPz = mu->p4().Pz();
				m_Mud0 = mu->primaryTrackParticle()->d0();
				m_MuMass = mu->p4().M();

				TLorentzVector dispLep1_tlv(0, 0, 0, 0);
				TLorentzVector dispLep2_tlv(0, 0, 0, 0);


				if (has2Mu) {
					m_Lep1Pt = subMuOpt1->pt() / 1000;
					m_Lep1Eta = subMuOpt1->eta();
					m_Lep1Phi = subMuOpt1->phi();
					m_Lep1Px = subMuOpt1->p4().Px();
					m_Lep1Py = subMuOpt1->p4().Py();
					m_Lep1Pz = subMuOpt1->p4().Pz();
					m_Lep1PtC = subMuOpt1->isolation(xAOD::Iso::ptcone30) / 1000;
					m_Lep1d0 = subMuOpt1->primaryTrackParticle()->d0();
					m_Lep1Mass = subMuOpt1->p4().M() / 1000;
					dispLep1_tlv = subMuOpt1->p4();


					m_Lep2Pt = subMuOpt2->pt() / 1000;
					m_Lep2Eta = subMuOpt2->eta();
					m_Lep2Phi = subMuOpt2->phi();
					m_Lep2Px = subMuOpt2->p4().Px();
					m_Lep2Py = subMuOpt2->p4().Py();
					m_Lep2Pz = subMuOpt2->p4().Pz();
					m_Lep2PtC = subMuOpt2->isolation(xAOD::Iso::ptcone30) / 1000;
					m_Lep2d0 = subMuOpt2->primaryTrackParticle()->d0();
					m_Lep2Mass = subMuOpt2->p4().M() / 1000;
					dispLep2_tlv = subMuOpt2->p4();
				} else if (has2Ele) {
					auto id_tr1 = xAOD::EgammaHelpers::getOriginalTrackParticle(subEleOpt1);
					m_Lep1Pt = subEleOpt1->pt() / 1000;
					m_Lep1Eta = subEleOpt1->eta();
					m_Lep1Phi = subEleOpt1->phi();
					m_Lep1Px = subEleOpt1->p4().Px();
					m_Lep1Py = subEleOpt1->p4().Py();
					m_Lep1Pz = subEleOpt1->p4().Pz();
					m_Lep1PtC = subEleOpt1->isolation(xAOD::Iso::ptcone30) / 1000;
					m_Lep1d0 = id_tr1->d0();
					m_Lep1Mass = subEleOpt1->p4().M() / 1000;
					dispLep1_tlv = subEleOpt1->p4();

					auto id_tr2 = xAOD::EgammaHelpers::getOriginalTrackParticle(subEleOpt2);
					m_Lep2Pt = subEleOpt2->pt() / 1000;
					m_Lep2Eta = subEleOpt2->eta();
					m_Lep2Phi = subEleOpt2->phi();
					m_Lep2Px = subEleOpt2->p4().Px();
					m_Lep2Py = subEleOpt2->p4().Py();
					m_Lep2Pz = subEleOpt2->p4().Pz();
					m_Lep2PtC = subEleOpt2->isolation(xAOD::Iso::ptcone30) / 1000;
					m_Lep2d0 = id_tr2->d0();
					m_Lep2Mass = subEleOpt2->p4().M() / 1000;
					dispLep2_tlv = subEleOpt2->p4();
				}

				if (hasEleMu) {
					m_Lep1Pt = subMuOpt1->pt() / 1000;
					m_Lep1Eta = subMuOpt1->eta();
					m_Lep1Phi = subMuOpt1->phi();
					m_Lep1Px = subMuOpt1->p4().Px();
					m_Lep1Py = subMuOpt1->p4().Py();
					m_Lep1Pz = subMuOpt1->p4().Pz();
					m_Lep1PtC = subMuOpt1->isolation(xAOD::Iso::ptcone30) / 1000;
					m_Lep1d0 = subMuOpt1->primaryTrackParticle()->d0();
					m_Lep1Mass = subMuOpt1->p4().M() / 1000;
					dispLep1_tlv = subMuOpt1->p4();

					auto id_tr = xAOD::EgammaHelpers::getOriginalTrackParticle(subEleOpt1);
					m_Lep2Pt = subEleOpt1->pt() / 1000;
					m_Lep2Eta = subEleOpt1->eta();
					m_Lep2Phi = subEleOpt1->phi();
					m_Lep2Px = subEleOpt1->p4().Px();
					m_Lep2Py = subEleOpt1->p4().Py();
					m_Lep2Pz = subEleOpt1->p4().Pz();
					m_Lep2PtC = subEleOpt1->isolation(xAOD::Iso::ptcone30) / 1000;
					m_Lep2d0 = id_tr->d0();
					m_Lep2Mass = subEleOpt1->p4().M() / 1000;
					dispLep2_tlv = subEleOpt1->p4();
				}
				Double_t invariantMass = (dispLep1_tlv + dispLep2_tlv).M();


				if (isMC) {
					m_DVMassTruth = invariantMass;
					bool matchPromptMuon = false;
					bool matchDispMu1 = false;
					bool matchDispMu2 = false;
					bool matchDispEl1 = false;
					bool matchDispEl2 = false;
					bool matchVertex = false;

					const xAOD::TruthParticle *promptMuTruth = xAOD::TruthHelpers::getTruthParticle(**lead_itr);

					if (promptMuTruth != 0) {
						int promptBarcode = promptMuTruth->barcode();
						for (auto itr = prompMu_barcode.begin(); itr != prompMu_barcode.end(); ++itr) {
							if (promptBarcode == *itr) {
								matchPromptMuon = true;
							}
						}
					}
					if (subMuOpt1 != 0) {
						const xAOD::TruthParticle *disp1MuTruth = xAOD::TruthHelpers::getTruthParticle(*subMuOpt1);

						if (disp1MuTruth != 0) {
							int disp1MuBarcode = disp1MuTruth->barcode();
							for (auto itr = dispLep_barcode.begin(); itr != dispLep_barcode.end(); ++itr) {
								if (disp1MuBarcode == *itr) {
									matchDispMu1 = true;
								}
							}
//							for (auto itr = dispLep2_barcode.begin(); itr != dispLep2_barcode.end(); itr++) {
//								if (disp1MuBarcode == *itr) {
//									matchDispMu1 = true;
//								}
//							}
						}
					}

					if (subMuOpt2 != 0) {
						const xAOD::TruthParticle *disp2MuTruth = xAOD::TruthHelpers::getTruthParticle(*subMuOpt2);

						if (disp2MuTruth != 0) {
							int disp2Barcode = disp2MuTruth->barcode();
							for (auto itr = dispLep_barcode.begin(); itr != dispLep_barcode.end(); ++itr) {
								if (disp2Barcode == *itr) {
									matchDispMu2 = true;
								}
							}
//							for (auto itr = dispLep2_barcode.begin(); itr != dispLep2_barcode.end(); itr++) {
//								if (disp2Barcode == *itr) {
//									matchDispMu2 = true;
//								}
//							}

						}
					}

					if (subEleOpt1 != 0) {
						const xAOD::TruthParticle *dispEleTruth = xAOD::TruthHelpers::getTruthParticle(*subEleOpt1);
						if (dispEleTruth != 0) {

							int dispEleBarcode = dispEleTruth->barcode();
							for (auto itr = dispEl_barcode.begin(); itr != dispEl_barcode.end(); ++itr) {
								if (dispEleBarcode == *itr) {
									matchDispEl1 = true;
								}
							}

						}
					}

					if (subEleOpt2 != 0) {
						const xAOD::TruthParticle *dispEleTruth = xAOD::TruthHelpers::getTruthParticle(*subEleOpt2);
						if (dispEleTruth != 0) {
							int dispEleBarcode = dispEleTruth->barcode();
							for (auto itr = dispEl_barcode.begin(); itr != dispEl_barcode.end(); ++itr) {
								if (dispEleBarcode == *itr) {
									matchDispEl2 = true;
								}
							}

						}
					}


					if (matchPromptMuon) {
						if (matchDispMu1) {
							const xAOD::TruthParticle *matchedDispMuon1 = xAOD::TruthHelpers::getTruthParticle(*subMuOpt1);
							if (matchedDispMuon1->hasProdVtx()) {
								const xAOD::TruthVertex *prodVtx = matchedDispMuon1->prodVtx();
								for (auto itr = dispVtx_barcode.begin(); itr != dispVtx_barcode.end(); ++itr) {
									if (prodVtx->barcode() == *itr) {
										matchVertex = true;
									}
								}
							}
						}
						if (matchDispMu2) {
							const xAOD::TruthParticle *matchedDispMuon2 = xAOD::TruthHelpers::getTruthParticle(*subMuOpt2);
							if (matchedDispMuon2->hasProdVtx()) {
								const xAOD::TruthVertex *prodVtx = matchedDispMuon2->prodVtx();
								for (auto itr = dispVtx_barcode.begin(); itr != dispVtx_barcode.end(); ++itr) {
									if (prodVtx->barcode() == *itr) {
										matchVertex = true;

									}
								}
							}
						}

						if (matchDispEl1) {
							const xAOD::TruthParticle *matchedDispElectron1 = xAOD::TruthHelpers::getTruthParticle(*subEleOpt1);
							if (matchedDispElectron1->hasProdVtx()) {
								const xAOD::TruthVertex *prodVtx = matchedDispElectron1->prodVtx();
								for (auto itr = dispVtx_barcode.begin(); itr != dispVtx_barcode.end(); ++itr) {
									if (prodVtx->barcode() == *itr) {
										matchVertex = true;
									}
								}
							}
						}
						if (matchDispEl2) {
							const xAOD::TruthParticle *matchedDispElectron2 = xAOD::TruthHelpers::getTruthParticle(*subEleOpt2);
							if (matchedDispElectron2->hasProdVtx()) {
								const xAOD::TruthVertex *prodVtx = matchedDispElectron2->prodVtx();
								for (auto itr = dispVtx_barcode.begin(); itr != dispVtx_barcode.end(); ++itr) {
									if (prodVtx->barcode() == *itr) {
										matchVertex = true;
									}
								}
							}
						}

					}
//					ATH_MSG_INFO("matches: " << matchPromptMuon << " and " << matchDispMu1 << " and " << matchDispMu2 << " and " << matchDispEl1 << " for " << m_eventNum);
//					if (matchPromptMuon && matchDispMu1 && (matchDispMu2 || matchDispEl1)) {
					if (matchVertex) {
						hnlTree->Fill();
					}

//					}
				} else {
					hnlTree->Fill();
				}


			}
		}
	}

	return
					StatusCode::SUCCESS;
}


xAOD::Muon *HNL::matchTrackToMuon(const xAOD::TrackParticle *track) {
	StatusCode resultsSC = StatusCode::SUCCESS;
	const xAOD::MuonContainer *muons = 0;
	resultsSC = m_storeGate->retrieve(muons, "Muons");
	xAOD::Muon *match = nullptr;
	typedef ElementLink<xAOD::TrackParticleContainer> trackParts;
	typedef ElementLink<xAOD::TruthParticleContainer> truthLink;
	const xAOD::TrackParticle *tr = *(track->auxdata<trackParts>("recoTrackLink"));
	std::pair<xAOD::MuonContainer *, xAOD::ShallowAuxContainer *> muons_shallowCopy = xAOD::shallowCopyContainer(
					*muons);
	xAOD::MuonContainer::iterator muonSC_itr = (muons_shallowCopy.first)->begin();
	for (; muonSC_itr != (muons_shallowCopy.first)->end(); ++muonSC_itr) {
		auto id_tr = (*muonSC_itr)->trackParticle(xAOD::Muon::InnerDetectorTrackParticle);
		if (id_tr == nullptr)continue;
		if (id_tr == tr) {
			match = *muonSC_itr;
		}

	}

	return match;
}

xAOD::Electron *HNL::matchTrackToElectron(const xAOD::TrackParticle *track) {
	StatusCode resultsSC = StatusCode::SUCCESS;
	const xAOD::ElectronContainer *electrons = 0;
	resultsSC = m_storeGate->retrieve(electrons, "Electrons");
	xAOD::Electron *match = nullptr;
	typedef ElementLink<xAOD::TrackParticleContainer> trackParts;
	typedef ElementLink<xAOD::TruthParticleContainer> truthLink;
	const xAOD::TrackParticle *tr = *(track->auxdata<trackParts>("recoTrackLink"));
	std::pair<xAOD::ElectronContainer *, xAOD::ShallowAuxContainer *> electrons_shallowCopy = xAOD::shallowCopyContainer(
					*electrons);
	xAOD::ElectronContainer::iterator electronSC_itr = (electrons_shallowCopy.first)->begin();
	for (; electronSC_itr != (electrons_shallowCopy.first)->end(); ++electronSC_itr) {
		auto id_tr = xAOD::EgammaHelpers::getOriginalTrackParticle(*electronSC_itr);
		if (id_tr == nullptr)continue;
		if (id_tr == tr) {
			match = *electronSC_itr;
		}

	}

	return match;
}


bool HNL::isLeadingMu(xAOD::Muon *leadingMuon) {

	if (leadingMuon == nullptr) {
		return false;
	}

	Double_t eta = (leadingMuon)->eta();
	Double_t ptcone30 = (leadingMuon)->isolation(xAOD::Iso::ptcone30) / 1000;
	Double_t pt = (leadingMuon)->pt() / 1000;

	bool ptCut = false;
	bool etaCut = false;
	bool ptcCut = false;
	bool combinedCut = false;

	combinedCut = (leadingMuon)->muonType() == xAOD::Muon::Combined;
	ptCut = pt > 28.0f;
	etaCut = fabs(eta) < 2.5f;
	ptcCut = (ptcone30 / pt < 0.05f);


	return (combinedCut && ptCut && etaCut && ptcCut);
}


bool HNL::isSubleadingMu(xAOD::Muon *subLeadingMuon) {

	if (subLeadingMuon == nullptr) {
		return false;
	}

	Double_t eta = (subLeadingMuon)->eta();
	Double_t ptcone30 = (subLeadingMuon)->isolation(xAOD::Iso::ptcone30) / 1000;
	Double_t pt = (subLeadingMuon)->pt() / 1000;
	float d0 = subLeadingMuon->primaryTrackParticle()->d0();
	float chi2 = 0.0f;
	int dof = 0;

	if (!subLeadingMuon->parameter(chi2, xAOD::Muon::msInnerMatchChi2)) {
		return false;
	}
	if (!subLeadingMuon->parameter(dof, xAOD::Muon::msInnerMatchDOF)) {
		return false;
	}

	float quality = chi2 / static_cast<float>(dof);

	bool ptCut = false;
	bool etaCut = false;
	bool ptcCut = false;
	bool standaloneTaggedCut = false;
	bool combinedQualityCut = false;
	bool combinedD0Cut = false;
	bool combinedCut = false;


	if (pt > 5) {
		ptCut = true;
	}
	if (abs(eta) < 2.5) {
		etaCut = true;
	}
	if (ptcone30 / pt < 1) {
		ptcCut = true;
	}

	bool paramsPass = (ptCut && etaCut && ptcCut);
	bool isStandaloneOrTagged = (subLeadingMuon)->muonType() == xAOD::Muon::MuonStandAlone ||
	                            (subLeadingMuon)->muonType() == xAOD::Muon::SegmentTagged;
	bool isCombinedMu = subLeadingMuon->muonType() == xAOD::Muon::Combined;
	bool highQualityWithD0 = quality < 5.0f && fabs(d0) > 0.1f;


	bool isCombinedWithD0 = isCombinedMu && (quality > 5 || highQualityWithD0);

	if (isStandaloneOrTagged) {
		standaloneTaggedCut = true;
	} else if (isCombinedMu) {
		//Now we check the fit quality
		if (quality > 5) {
			combinedQualityCut = true;
		} else if (quality <= 5 && fabs(d0) > 0.1f) {
			combinedD0Cut = true;
		}
		combinedCut = (combinedD0Cut || combinedQualityCut);
	}

	if (combinedCut && !isCombinedWithD0) {
		ATH_MSG_INFO("Problem in " << m_eventNum);
	}

	bool subleadingTest = paramsPass && (isStandaloneOrTagged || isCombinedWithD0);
	if ((ptCut && etaCut && ptcCut && (standaloneTaggedCut || combinedCut)) && !subleadingTest) {
		ATH_MSG_INFO("There's a problem in the logic here " << m_eventNum);
	}
	return (ptCut && etaCut && ptcCut && (standaloneTaggedCut || combinedCut));
}

bool HNL::testDV(const xAOD::Vertex *displacedVertex) {
	int nmuon = 0;
	int nloosemuon = 0;
	int nmediummuon = 0;
	int ntightmuon = 0;
	int nelectron = 0;
	int nlooseelectron = 0;
	int nmediumelectron = 0;
	int ntightelectron = 0;


	bool vtxHas1Mu = false;
	bool vtxHas2Mu = false;

	bool vtxHas1MuTight = false;
	bool vtxHas2MuTight = false;

	const xAOD::MuonContainer *muons = 0;
	m_storeGate->retrieve(muons, "Muons");

	const xAOD::ElectronContainer *electrons = 0;
	m_storeGate->retrieve(electrons, "Electrons");
//    const xAOD::ElectronContainer *electrons = 0;
//    m_storeGate->retrieve(electrons, "Electrons");

	TLorentzVector track1_vector(0, 0, 0, 0);
	TLorentzVector track2_vector(0, 0, 0, 0);
	double totalEta = 0;
	float vertexM = displacedVertex->auxdataConst<float>("vtx_mass");
	auto dv_pos = displacedVertex->position();
	double rDV = dv_pos.perp();
	if ((rDV > 3 && (rDV < 400))) {
		PassFiducialCut = true;
	}
	if (displacedVertex->nTrackParticles() == 2) {
		Pass2TrackDV = true;
	} else {
		return false;
	}

	int DVsign = 1;
	for (size_t tk = 0; tk < displacedVertex->nTrackParticles(); ++tk) {
		const xAOD::TrackParticle *dvTrk = displacedVertex->trackParticle(tk);
		int tk_charge = (displacedVertex->trackParticle(tk))->charge();
		DVsign = DVsign * tk_charge;

		totalEta += dvTrk->eta();
		if (tk == 0) {
			track1_vector = dvTrk->p4();
		} else {
			track2_vector = dvTrk->p4();
		}

		const xAOD::TrackParticle *tr = *dvTrk->auxdata<ElementLink<xAOD::TrackParticleContainer> >("recoTrackLink");
		// create a shallow copy of the muons container
		std::pair<xAOD::MuonContainer *, xAOD::ShallowAuxContainer *> muons_shallowCopy = xAOD::shallowCopyContainer(
						*muons);
		// iterate over our shallow copy
		xAOD::MuonContainer::iterator muonSC_itr = (muons_shallowCopy.first)->begin();
		xAOD::MuonContainer::iterator muonSC_end = (muons_shallowCopy.first)->end();
		for (; muonSC_itr != muonSC_end; ++muonSC_itr) {
			if (m_muonCalibrationAndSmearingTool->applyCorrection(**muonSC_itr) == CP::CorrectionCode::Error) {
				Error("execute()", "MuonCalibrationAndSmearingTool returns Error CorrectionCode");
			}
			auto id_tr = (*muonSC_itr)->trackParticle(xAOD::Muon::InnerDetectorTrackParticle);
			if (id_tr == nullptr)continue;
			if (id_tr == tr) {

				if (vtxHas1Mu) {//if we already counted one muon from this vertex
					vtxHas2Mu = true;
				} else {
					//if not, then this is the first muon we count in this vertex
					vtxHas1Mu = true;
				}
				nmuon++;
				if (m_muonTightSelection->accept(**muonSC_itr)) {//One of the muons in the DV is a tight muon
					ntightmuon++;
					if (vtxHas1MuTight) {//if we already counted one tight muon from this vertex
						vtxHas2MuTight = true;
					} else {
						//if not, then this is the first tight muon we count in this vertex
						vtxHas1MuTight = true;
					}
				}
			}
		}

		delete muons_shallowCopy.first;
		delete muons_shallowCopy.second;

		//For electrons
		xAOD::ElectronContainer::const_iterator electron_itr = electrons->begin();
		xAOD::ElectronContainer::const_iterator electron_end = electrons->end();

		for (; electron_itr != electron_end; ++electron_itr) {
			const xAOD::TrackParticle *TrackPart = (*electron_itr)->trackParticle();
			TLorentzVector tlv = TrackPart->p4();
			auto id_tr = xAOD::EgammaHelpers::getOriginalTrackParticle(*electron_itr);
			if (id_tr == nullptr)continue;
			if (id_tr == tr) {
				nelectron++;
				if (m_TightLH->accept(*electron_itr)) {
					ntightelectron++;
					PassTightElectron = true;
				}


			}
		}

	}
	double deltaPhi = track1_vector.DeltaPhi(track2_vector);
	double cosmicVeto = sqrt(totalEta * totalEta + (M_PI - deltaPhi) * (M_PI - deltaPhi));

	if (cosmicVeto > 0.04) {
		PassCosmicVeto = cosmicVeto > 0.04;
	}
	if (DVsign == -1) {
		Pass2TrackOSDV = DVsign == -1;
	}
	if (vtxHas1Mu) {
		Pass1MuonDV = true;
	}
	if (vtxHas1MuTight) {
		Pass1TightMuonDV = true;
	}
	if (vtxHas2Mu || (vtxHas1Mu && nelectron == 1)) {
		Pass2LeptonDV = true;
	}
	if (vtxHas2MuTight || (vtxHas1MuTight && PassTightElectron)) {
		Pass2TightLeptonDV = true;
	}

	if (vertexM > 4000) {//4000 MeV
		PassDVMassCut = true;
	}

	return (PassFiducialCut && Pass2TrackDV && Pass2TrackOSDV && Pass1MuonDV && Pass1TightMuonDV && PassDVMassCut &&
	        PassCosmicVeto);
}

void HNL::resetParams() {

	m_MuPt = -999;
	m_MuEta = -999;
	m_MuPhi = -999;
	m_MuPx = -999;
	m_MuPy = -999;
	m_MuPz = -999;
	m_MuPtC = -999;
	m_Mud0 = -999;

	m_Lep1Pt = -999;
	m_Lep1Eta = -999;
	m_Lep1Phi = -999;
	m_Lep1Px = -999;
	m_Lep1Py = -999;
	m_Lep1Pz = -999;
	m_Lep1PtC = -999;
	m_Lep1d0 = -999;

	m_Lep2Pt = -999;
	m_Lep2Eta = -999;
	m_Lep2Phi = -999;
	m_Lep2Px = -999;
	m_Lep2Py = -999;
	m_Lep2Pz = -999;
	m_Lep2PtC = -999;
	m_Lep2d0 = -999;

	m_HNLrDV = -999;
	m_DVMass = -999;


	m_MuPtTruth = -999;
	m_MuEtaTruth = -999;
	m_MuPhiTruth = -999;

	m_Lep1PtTruth = -999;
	m_Lep1EtaTruth = -999;
	m_Lep1PhiTruth = -999;
	m_Lep1d0Truth = -999;

	m_Lep2PtTruth = -999;
	m_Lep2EtaTruth = -999;
	m_Lep2PhiTruth = -999;
	m_Lep2d0Truth = -999;

	m_HNLrDVTruth = -999;

	m_DVMassTruth = -999;

	PassTrigger = false;
	PassPromtMu = false;
	PassDispMu = false;

	PassDRAW = false;

	//Signal (A)
	PassTightPromptMuon = false;
	PassMaterialVeto = false;
	PassFiducialCut = false;
	Pass2TrackDV = false;
	Pass2TrackOSDV = false;
	Pass1MuonDV = false;
	Pass1TightMuonDV = false;
	Pass2LeptonDV = false;
	Pass2TightLeptonDV = false;
	PassCosmicVeto = false;
	PassDVMassCut = false;

	PassDVPresence = false;

	m_CosmicVeto = -999;
	m_cosmicPassed = -999;

}