
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../HNL.h"

DECLARE_ALGORITHM_FACTORY( HNL )

DECLARE_FACTORY_ENTRIES( HNL ) 
{
  DECLARE_ALGORITHM( HNL );
}
